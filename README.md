# ProtopiaEcosystem utilities

Библиотека для  [ProtopiaEcosystem React Client](http://ux.protopia-home.ru/)

Данный пакет автоматически устанавливается в **Create-react-app приложение** и предоставляет разработчикам сторонних модулей необходимый набор утилит.

## Визуальные Компоненты

### Loading

Простой компонент визуализации процесса загрузки. Вращающаяся "подковка"

``` 
import React from "react"
import { Loading } from "react-pe-utilities"

const App = props => {
    return <Loading />
} 
export default App
```

## Переводчик 

### translitterate

Транслиттерация кириллического текста

``` 
import React from "react"
import { translitterate } from "react-pe-utilities"

const App = props => {
    const text = translitterate('ru').transform("строка").toLowerCase()
    return <> { text } </>
} 
export default App
```

### __

Перевод строки (или идентификатора) из словаря, зарегистрированного командой *initDictionary* на текущий язык.

``` 
import React from "react"
import { __ } from "react-pe-utilities"

const App = props => {
    const text = __( "Loading" )
    return <> { text } </>
} 
export default App
```

### sprintf

Подстановка подстрок в строку, указанную первым аргументом. Заменяет подстроки вида **%s** на слудующие за первым аргументы: первое вхождение подстроки **%s** заменяется на второй строковый аргумент, второе вхождение — на третий аргумент итд.

``` 
import React from "react"
import { __, sprintf } from "react-pe-utilities"

const App = props => {
    const url = "http://ux.protopia-home.ru/"
    const text = sprintf( __( "see url: %s" ), url)
    return <> { text } </>
} 
export default App
```

###  initDictionary

Инициализация словаря в формате json. Указываем в аргументах:

1.  идентификатор языка (стока в формате i18n)

2.  JSON (в формате i18n)

``` 
import React, { useEffect } from "react"
import { __, initDictionary } from "react-pe-utilities"

const App = props => {
    useEffect(() =>
    {
        initDictionary('ru', props.dictionary)
    }, [])
    return <> { __( "Title" ) } </>
} 
export default App
```

## Структурные 

###  importAll

-------

###  getWidgets

Массив Компонентов Виджетов, доступных в данном Приложении

###  initWidgets

Регистрируем виджеты сторонних модулей

###  getWidget

Получаем Компонент Виждета по строке...


###  initArea

Монтируем в BABEL-код гнездо визуальных Виджетов из списка доступных в приложении гнёзд (предоставляются Модулями)

###  initDataArea

Монтируем в BABEL-код гнездо Виджетов Данных из списка доступных в приложении гнёзд (предоставляются Модулями)

###  widgetAreas

Список гнёзд Виджетов данного приложения

## Утилиты текущего Пользователя 

### isLoggedPage

Возвращает булевое значение. Есть ли  в Карте сайта страница входа Пользователя

### userModel

Возвращает тип данных «Пользователь» приложения

### userModelField

Возвращает список полей Типа данных  «Пользователь» приложения

### userModelKeys

Возвращает список названий (ключей) полей Типа данных  «Пользователь» приложения

### isCapability

Возвращает булевое значение. Доступно ли текущему Пользователю указанное право?

### isRole

Возвращает булевое значение. Есть ли указанная Роль у текущего Пользователя?

## Навигационные

### scrollToElement

### scrollToElementByJQuery