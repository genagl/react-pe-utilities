import i18next from "i18next"
let ru_orig = {};
export const initDictionary = (lang, src) =>
{
  ru_orig = { ...src }
  i18next
    .init({
      keySeparator: "",
      nsSeparator: "",
      interpolation: {
        // React already does escaping
        escapeValue: false,

      },
      lng: "ru", // 'en' | 'ru'
      // Using simple hardcoded resources for simple example
      resources: {
        ru: {
          translation: ru_orig,

        },

      },
    })
  return true
}
export default i18next
export function __(text) {
  return i18next.t(text)
}
