import {getAllRoutes} from "react-pe-layouts";

export function data_type_link_url(data_type_class_name) {
    if( data_type_class_name)
    {
      const routeObj = getAllRoutes().filter(route => route.singled_data_type == data_type_class_name)[0]
      return routeObj ? routeObj.route : null
    }   
    return null
  }
  
  export function data_type_feed_url(data_type_class_name) {
    if( data_type_class_name)
    {
      const routeObj = getAllRoutes().filter(route => route.feed_data_type == data_type_class_name)[0]
      return routeObj ? routeObj.route : null
    }   
    return null
  }