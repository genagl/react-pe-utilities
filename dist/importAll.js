import Layouts from "react-pe-layouts";
import ReactDynamicImport from "react-dynamic-import";
export default function importAll(r, exists = [], components = {}, lasies = {}, prefix = "./") {
  r.keys().forEach(key => {
    let key1 = key.replace("./", "").split(".").slice(0, -1).join(".").split("/");
    key1 = key1[key1.length - 1];

    if (exists.length === 0 || exists.filter(elem => elem === key1).length > 0) {
      components[key1] = r(key); //console.log( prefix + key.replace("./", "") )

      lasies[key1] = prefix + key.replace("./", "");
    }
  });
}
export function getModules() {
  return Layouts() && Layouts().module ? Layouts().module : {};
}
export function getAllViews(components = {}) {
  const modules = getModules();
  Object.keys(modules).forEach(module => {
    Object.keys(modules[module].views).forEach(view => {
      const loader = f => import(`${module}/${f}`);

      components[view] = ReactDynamicImport({
        name: `${view}`,
        loader,
        isHOC: true
      });
      console.log(`${module}/${f}`);
    });
  });
  console.log(components);
}
export function getAllWidgets(components = {}) {
  const modules = getModules();
  Object.keys(modules).forEach(module => {
    Object.keys(modules[module].widgets).forEach(widget => {
      const loader = f => import(`${module}`);

      components[widget] = ReactDynamicImport({
        name: `${widget}`,
        loader
      });
    });
  });
}